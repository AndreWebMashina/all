const swiperCatalog = new Swiper('.swiper-catalog', {
    // Navigation arrows
    navigation: {
        nextEl: '.catalog-button-next',
        prevEl: '.catalog-button-prev',
    },
    slidesPerView: 1,

    breakpoints: {

        600: {
            slidesPerView: 2
        },
        850: {
            slidesPerView: 3
        },
        1180: {
            slidesPerView: 4
        },
        1400: {
            slidesPerView: 5
        }
    }
});

const swiperReviews = new Swiper('.swiper-reviews', {
    // Navigation arrows
    navigation: {
        nextEl: '.reviews-button-next',
        prevEl: '.reviews-button-prev',
    },
    // effect: "cards",
    slidesPerView: 1,
    spaceBetween: 10,
    // autoplay: true,
    loop: true,
    grabCursor: true,

})