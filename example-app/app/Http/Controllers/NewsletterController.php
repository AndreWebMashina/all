<?php

namespace App\Http\Controllers;

use Exception;
use App\Services\Newsletter;
use Illuminate\Validation\ValidationException;

class NewsletterController extends Controller
{


    public function __invoke(Newsletter $newsletter)
    {
        request()->validate([
            'email' => 'required|email'
        ]);

        try {
            $newsletter->subscribe(request('email'));
        } catch (Exception $e) {
           throw ValidationException::withMessages([
                'email' => 'invalid email'
            ]);
        }

        return redirect('/')->with('success', 'You are subscribed for news');
    }
}
