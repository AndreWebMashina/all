<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Validation\Rule;

class PostController extends Controller
{
    public function index()
    {
        return view('home', [
            'posts' => Post::latest()->filter(request(['search', 'category', 'author']))
                ->paginate(9)->withQueryString()]);
    }

    public function show(Post $post)
    {
        return view('post', ['post' => $post]);
    }

    public function create()
    {
        return view('create');
    }

    public function store()
    {
        $attributes = request()->validate([
            'title' => 'required|min:2',
            'thumbnail' => 'image',
            'except' => 'required',
            'body' => 'required',
            'category_id' => ['required', Rule::exists('categories', 'id')]
        ]);

        $attributes['user_id'] = auth()->id();
        if (request()->hasFile('thumbnail')){
            $attributes['thumbnail']= request()->file('thumbnail')->store('thumbnail');
        }

        Post::create($attributes);

        return redirect('/');
    }
}
