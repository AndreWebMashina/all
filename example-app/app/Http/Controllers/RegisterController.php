<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function create()
    {
        return view('register.create');
    }

    public function store()
    {
        $attributes = request()->validate([
            'user_name' => 'required|unique:users,user_name|min:2|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => ['required', 'min:6', 'max:255']
        ]);

        $user=User::create($attributes);
//        auth()->login($user);

        return redirect('/login')->with('success', 'You create a account!');
    }
}
