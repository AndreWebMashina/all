<head>
    <title>home</title>
</head>
<x-layout>

    @include('_header')

    <main class="max-w-6xl mx-auto mt-6 lg:mt-20 space-y-6">
        @if($posts->count())
            <x-post-grid :posts="$posts"/>
            {{$posts->links()}}
        @else
            <p class="text-center">NO POSTS</p>
        @endif
    </main>

    {{--    <main>--}}
    {{--        @foreach ($posts as $post)--}}
    {{--            <article>--}}
    {{--                <h2><a href="/post/postN{{$post->id}}">{{$post->title}}</a></h2>--}}
    {{--                <div>{{$post->except}}</div>--}}
    {{--                <span>By <a href="/user/{{$post->author->id}}"><strong>{{$post->author->user_name}}</strong></a> from <a--}}
    {{--                        href="/categories/{{$post->category->slug}}"><strong>{{$post->category->name}}</strong> category</a></span>--}}
    {{--            </article>--}}
    {{--        @endforeach--}}
    {{--    </main>--}}
</x-layout>
