<x-layout>
    <main class="max-w-lg mx-auto mt-10">
        <h1>LOG IN</h1>
        <form method="POST" action="/sessions" class="mt-10">
            @csrf
            <div class="mb-6">
                <label for="email" class="block mb-2 text-gray-700">Email</label>
                <input type="email"
                       name="email"
                       placeholder="Email"
                       id="email"
                       value="{{old('email')}}"
                       required
                       class="border border-gray-400 p-2 w-full">
                @error('email')
                <p class="text-red-500">{{$message}}</p>
                @enderror
            </div>
            <div class="mb-6">
                <label for="password" class="block mb-2 text-gray-700">Password</label>
                <input type="password"
                       name="password"
                       placeholder="Password"
                       id="password"
                       required
                       class="border border-gray-400 p-2 w-full">
                @error('password')
                <p class="text-red-500">{{$message}}</p>
                @enderror
            </div>
            <div class="mb-6">
                <button type="submit" class="bg-blue-500 hover:bg-blue-400 rounded py-2 px-4 text-white">Log In</button>
            </div>
        </form>
    </main>
</x-layout>
