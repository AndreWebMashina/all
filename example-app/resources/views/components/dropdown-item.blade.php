
@php
$classes='block hover:bg-red-300 focus:bg-red-300';
@endphp

<a {{$attributes(['class'=>$classes])}}>{{$slot}}</a>
