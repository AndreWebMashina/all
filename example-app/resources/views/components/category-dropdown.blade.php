<x-dropdown>
    <x-slot name="trigger">
        <button class="inline-flex w-32">
            {{isset($currentCategory)?ucwords($currentCategory->name):'Categories'}}
            <x-icon name="down-arrow" style="right: 12px;"/>
        </button>
    </x-slot>

    <x-dropdown-item href="/?&{{http_build_query(request()->except('category', 'page'))}}" class="{{request('category')?'':'bg-red-300'}}">All</x-dropdown-item>
    @foreach($categories as $category)
        <x-dropdown-item
            class="{{(request('category')==$category->slug)?'bg-red-300':''}}"
            href="/?category={{$category->slug}}&{{http_build_query(request()->except('category', 'page'))}}">
            {{ucwords($category->name)}}</x-dropdown-item>
    @endforeach
</x-dropdown>
