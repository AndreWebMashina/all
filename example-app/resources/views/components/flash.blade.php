
@if(session()->has('success'))
    <div x-data="{show:true}"
         x-init="setTimeout(()=>show=false, 3500)"
         x-show="show"
         class="py-4 px-4 bg-blue-500 fixed text-white bottom-5 right-5 rounded">
        <p>{{session('success')}}</p>
    </div>
@endif
