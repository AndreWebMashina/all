<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\User;
use App\Models\Post;
use App\Models\Comments;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $user = User::factory()->create([
//            'user_name' => 'Andrei Ka'
//        ]);

//        Post::factory(5)->create([
//            'author_id' => $user->id
//        ]);
        User::factory(6)->create();
        Category::factory(7)->create();
        Post::factory(59)->create();
        Comments::factory(333)->create();
    }
}
